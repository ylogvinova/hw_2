const fs = require('fs')

function writeToFile(argv) {
  const fileName = argv[2]
  const content = argv.slice(3).join(' ')

  // sync blocking
  // try {
  //   fs.writeFileSync(fileName, content, { flag: 'a+' })
  //   console.log("File was created/updated");
  // } catch (err) {
  //   console.error('Ooops, something went wrong', err)
  // }

  // async non-blocking
  fs.writeFile(fileName, content, { flag: 'a+' }, (err) => {
    if (err) throw new Error('Ooops, something went wrong');
    console.log("File was created/updated");
  })
}

writeToFile(process.argv)
